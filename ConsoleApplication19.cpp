﻿

#include <iostream>

class Animal
{
private:
    int x;
public:
    Animal(){}
    Animal(int _x) :x(_x)
    {}
    virtual void Voice()
    {
        std::cout << "I am animal";
    }

};



class Dog : public Animal
{
private:
    int y;
public:
    Dog (){}
    Dog(int _y, int _x):y(_y),Animal(_x)
    {}
    void Voice() override
    {
        std::cout << "Woof Woof" << '\n';
    }

};
class Cat : public Animal
{
private:
    int z;
public:
    Cat(){}
    Cat(int _z, int _x):z(_z),Animal(_x)
    {}
    void Voice() override
    {
        std::cout << "Miaw Miaw" << '\n';
    }
};
class Cow : public Animal
{
private:
    int s;
public:
    Cow(){}
    Cow(int _s, int _x):s(_s), Animal(_x)
    {}
    void Voice() override
    {
        std::cout << "Muuu Muuu"<<'\n';
    }
};
int main()
{
    const int x = 3;
    Animal* array[x] = { new Dog(), new Cat(), new Cow() };
 

    for (int i = 0; i < x; i++)
    {
       array[i] -> Voice();
    }
}

